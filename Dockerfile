FROM alpine:3.7

ARG HUGO_VERSION
ARG HUGO_SHA

RUN apk --no-cache add \
      openssh-client \
      openssl \
      rsync \
    && wget -O $HUGO_VERSION.tar.gz https://github.com/gohugoio/hugo/releases/download/v$HUGO_VERSION/hugo_${HUGO_VERSION}_Linux-64bit.tar.gz \
    && echo "$HUGO_SHA  $HUGO_VERSION.tar.gz" | sha256sum -c \
    && tar xf $HUGO_VERSION.tar.gz && mv hugo* /usr/bin/hugo
