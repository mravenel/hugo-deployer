import feedparser
import requests
import subprocess
import sys

from datetime import datetime

HUGO_RELEASES = "https://github.com/gohugoio/hugo/releases.atom"
PARSED_FEED = feedparser.parse(HUGO_RELEASES)
CHECKSUM_TEMPLATE = "https://github.com/gohugoio/hugo/releases/download/v{{ version }}/hugo_{{ version }}_checksums.txt"
FILE_NAME_TEMPLATE = "hugo_{{ version }}_Linux-64bit.tar.gz"


def last_updated():
    updated_time = datetime.strptime(PARSED_FEED.feed.updated, "%Y-%m-%dT%H:%M:%SZ")
    return updated_time


def new_version_published():
    return True #(datetime.today() - last_updated()).days < 1


def versions():
    hugo_versions = set()
    for entry in PARSED_FEED['entries']:
        hugo_versions.add(entry['title'].replace('v', ''))
    return hugo_versions


def get_checksum(version):
    u = CHECKSUM_TEMPLATE.replace("{{ version }}", version)
    r = requests.get(u)
    lines = r.text.split("\n")
    filename = FILE_NAME_TEMPLATE.replace("{{ version }}", version)

    for line in lines:
        if filename in line:
            return line.split(' ')[0]


def docker_build(name, tag, version, checksum):
    retcode = subprocess.call(["docker", "build",
                               "-t", name + ":" + tag,
                               "--build-arg", "HUGO_VERSION=" + version,
                               "--build-arg", "HUGO_SHA=" + checksum,
                               "."])

    if retcode != 0:
        exit(retcode)


def docker_push(name, tag):
    retcode = subprocess.call(["docker", "push", name + ":" + tag])
    if retcode != 0:
        exit(retcode)


def main():
    if new_version_published():
        v = versions()
        latest = max(v)
        image_name = "hugo"

        if len(sys.argv) == 2:
            image_name = sys.argv[1]

        for version in v:
            checksum = get_checksum(version)
            docker_build(image_name, version, version, checksum)
            docker_push(image_name, version)

            if version == latest:
                docker_build(image_name, "latest", version, checksum)
                docker_push(image_name, "latest")
    else:
        print("Hugo images up to date.")


if __name__ == '__main__':
    main()
